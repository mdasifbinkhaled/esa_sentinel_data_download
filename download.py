import datetime
import os
import shutil
import time

from progressbar import Bar, Percentage, ProgressBar
from sentinelsat import SentinelAPI


def moveProduct(filename, target_root_directory, year, month):

    if not os.path.exists("./{}/{}/{}/{}".format(target_root_directory,filename.split("_")[0], year, month)):
        os.makedirs("./{}/{}/{}/{}".format(target_root_directory,filename.split("_")[0], year, month))
        print("./{}/{}/{}/{} Created".format(filename.split("_")[0],target_root_directory, year, month))

    shutil.move("./{}".format(filename), "./{}/{}/{}/{}/{}".format(target_root_directory,filename.split("_")[0], year, month, filename))
    print("Moved {} to {}".format(filename,"./{}/{}/{}/{}/{}".format(target_root_directory,filename.split("_")[0], year, month, filename)))


def updateList(api, input_filename,target_root_directory):
    with open(input_filename) as file:
        content = file.readlines()
    formatted_content = [x.strip() for x in content]
    formatted_content_temporary = formatted_content.copy()
    for product_id in formatted_content_temporary:
        product_info = api.get_product_odata(product_id)

        if os.path.exists("{}.zip.incomplete".format(product_info['title'])):
            os.remove("{}.zip.incomplete".format(product_info['title']))
            print("{}.zip.incomplete Deleted.".format(product_info['title']))

        if ("{}.zip".format(product_info['title']) in os.listdir('.')):
            moveProduct("{}.zip".format(product_info['title']), target_root_directory, product_info['date'].year,
                        product_info['date'].strftime("%B"))
        for root, directory, file in os.walk(target_root_directory):
            if "{}.zip".format(product_info['title']) in file:
                print("{}.zip is Already Downloaded. Removing from the List.".format(product_info['title']))
                formatted_content.remove(product_id)
    return formatted_content


def calculateElapsedTime(starting_time):
    return datetime.datetime.now() - starting_time


def download(input_filename, username, password, url, interval, target_root_directory):
    starting_time = datetime.datetime.now()
    api = SentinelAPI(username, password, url)
    formatted_content = updateList(api, input_filename,target_root_directory)
    print("{} Product(s) Remaining".format(len(formatted_content)))
    index = 0
    while not formatted_content == []:
        starting_time_individual = datetime.datetime.now()
        product_info = api.get_product_odata(formatted_content[index])
        try:
            api.download(formatted_content[index])
        except Exception:
            # this delay is introduced to let all the error to be printed without any disruption
            time.sleep(1)
            pass

        if "{}.zip".format(product_info['title']) in os.listdir('.'):
            print("{}.zip is Downloaded. Removing from the List.".format(product_info['title']))
            formatted_content.remove(formatted_content[index])
            moveProduct("{}.zip".format(product_info['title']), target_root_directory, product_info['date'].year,
                        product_info['date'].strftime("%B"))

        if not product_info['Online']:
            print("Pausing for {} minutes".format(interval / 60))
            # time.sleep(interval)
            progressBar = ProgressBar(widgets=[Percentage(), Bar()], maxval=300).start()
            for i in range(300):
                time.sleep(interval / 300)
                progressBar.update(i + 1)
            progressBar.finish()
        if (len(formatted_content) == 0):
            break;
        else:
            index = (index + 1) % len(formatted_content)
            print("{} Products Remaining".format(len(formatted_content)))
        print("Elapsed Time: {}".format(calculateElapsedTime(starting_time_individual)))
    print("Total Elapsed Time: {}".format(calculateElapsedTime(starting_time)))


if __name__ == "__main__":
    # credentials
    username = 'mdasifbinkhaled'
    password = '1982gonzo'
    # attributes
    url = 'https://scihub.copernicus.eu/dhus'
    input_filename = 'product_ids.txt'
    target_root_directory = "sentinel_data"
    interval = 20 * 60
    # start downloading
    download(input_filename, username, password, url, interval, target_root_directory)

